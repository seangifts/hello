import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.io.File;

// 
// Decompiled by Procyon v0.5.30
// 

public class SQLVars
{
    private static File file;
    private static List<String> fileData;
    private static ConcurrentHashMap<String, Integer> colPos;
    private static ConcurrentHashMap<Integer, String> sqls;
    
    static {
        SQLVars.fileData = new ArrayList<String>();
        SQLVars.colPos = new ConcurrentHashMap<String, Integer>();
        SQLVars.sqls = new ConcurrentHashMap<Integer, String>();
    }
    
    public static File getFile() {
        return SQLVars.file;
    }
    
    public static void setFile(final File file) {
        SQLVars.file = file;
    }
    
    public static List<String> getFileData() {
        return SQLVars.fileData;
    }
    
    public static void setFileData(final List<String> fileData) {
        SQLVars.fileData = fileData;
    }
    
    public static ConcurrentHashMap<String, Integer> getColPos() {
        return SQLVars.colPos;
    }
    
    public static void setColPos(final ConcurrentHashMap<String, Integer> colPos) {
        SQLVars.colPos = colPos;
    }
    
    public static ConcurrentHashMap<Integer, String> getSqls() {
        return SQLVars.sqls;
    }
    
    public static void setSqls(final ConcurrentHashMap<Integer, String> sqls) {
        SQLVars.sqls = sqls;
    }
}