// 
// Decompiled by Procyon v0.5.30
// 

public class SQLConfig
{
    public static void main(final String[] array) {
        final SQLUtils sqlUtils = new SQLUtils();
        sqlUtils.setConfigFile("runner.cfg");
        if (array[0].equalsIgnoreCase("Setup")) {
            sqlUtils.mkSQLConfig();
        }
        else if (array[0].equalsIgnoreCase("insert")) {
            sqlUtils.insertData(array[1]);
        }
        else if (array[0].equalsIgnoreCase("run")) {
            sqlUtils.runSQL(array[1]);
        }
        else if (array[0].equalsIgnoreCase("clean")) {
            sqlUtils.runShell("clean");
        }
    }
}