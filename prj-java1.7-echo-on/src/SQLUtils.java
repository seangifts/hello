import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.io.Reader;
import java.io.FileReader;
import java.util.concurrent.ConcurrentHashMap;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;
import java.io.BufferedReader;

// 
// Decompiled by Procyon v0.5.30
// 

public class SQLUtils
{
    private BufferedReader b;
    private static final String SQLRUN = "RUN";
    private static final String SQLCOMPLETED = "COMPLETED";
    private String s;
    private String s2;
    
    public void setConfigFile(final String s) {
        SQLVars.setFile(new File(s));
    }
    
    public void mkSQLConfig() {
        try {
            if (SQLVars.getFile().createNewFile()) {
                System.out.println("File is created!");
                this.mkHeaders();
            }
            else {
                System.out.println("File already exists ...................");
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void mkHeaders() {
        Writer writer = null;
        Writer writer2 = null;
        this.setS("SQL_SCRIPT|SIT|UAT|PREPROD|PROD|SQL_CONTENT");
        try {
            writer2 = new FileWriter(SQLVars.getFile().getAbsoluteFile(), true);
            writer = new BufferedWriter(writer2);
            writer.write("SQL_SCRIPT|SIT|UAT|PREPROD|PROD|SQL_CONTENT");
            ((BufferedWriter)writer).newLine();
            System.out.println("Header Generated ==================== SQL_SCRIPT|SIT|UAT|PREPROD|PROD|SQL_CONTENT");
        }
        catch (IOException ex) {
            ex.printStackTrace();
            try {
                if (writer != null) {
                    ((BufferedWriter)writer).close();
                }
                if (writer2 != null) {
                    ((OutputStreamWriter)writer2).close();
                }
            }
            catch (IOException ex2) {
                ex2.printStackTrace();
            }
            try {
                if (writer != null) {
                    ((BufferedWriter)writer).close();
                }
                if (writer2 != null) {
                    ((OutputStreamWriter)writer2).close();
                }
            }
            catch (IOException ex3) {
                ex3.printStackTrace();
            }
            return;
        }
        finally {
            try {
                if (writer != null) {
                    ((BufferedWriter)writer).close();
                }
                if (writer2 != null) {
                    ((OutputStreamWriter)writer2).close();
                }
            }
            catch (IOException ex4) {
                ex4.printStackTrace();
            }
        }
        try {
            if (writer != null) {
                ((BufferedWriter)writer).close();
            }
            if (writer2 != null) {
                ((OutputStreamWriter)writer2).close();
            }
        }
        catch (IOException ex4) {
            ex4.printStackTrace();
        }
        try {
            if (writer != null) {
                ((BufferedWriter)writer).close();
            }
            if (writer2 != null) {
                ((OutputStreamWriter)writer2).close();
            }
        }
        catch (IOException ex5) {
            ex5.printStackTrace();
        }
    }
    
    public void insertData(final String s) {
        this.getData();
        this.getSql(s);
        for (int i = 0; i < SQLVars.getSqls().size(); ++i) {
            if (this.vrfyInsrt(SQLVars.getSqls().get(i)) < 1) {
                Writer writer = null;
                Writer writer2 = null;
                final String string = String.valueOf(String.valueOf(s)) + "|" + "RUN" + "|" + "RUN" + "|" + "RUN" + "|" + "RUN" + "|" + SQLVars.getSqls().get(i);
                try {
                    writer2 = new FileWriter(SQLVars.getFile().getAbsoluteFile(), true);
                    writer = new BufferedWriter(writer2);
                    writer.write(string);
                    ((BufferedWriter)writer).newLine();
                    System.out.println("Record Inserted ==================== " + string);
                }
                catch (IOException ex) {
                    ex.printStackTrace();
                    try {
                        if (writer != null) {
                            ((BufferedWriter)writer).close();
                        }
                        if (writer2 != null) {
                            ((OutputStreamWriter)writer2).close();
                        }
                    }
                    catch (IOException ex2) {
                        ex2.printStackTrace();
                    }
                    try {
                        if (writer != null) {
                            ((BufferedWriter)writer).close();
                        }
                        if (writer2 != null) {
                            ((OutputStreamWriter)writer2).close();
                        }
                    }
                    catch (IOException ex3) {
                        ex3.printStackTrace();
                    }
                    try {
                        if (writer != null) {
                            ((BufferedWriter)writer).close();
                        }
                        if (writer2 != null) {
                            ((OutputStreamWriter)writer2).close();
                        }
                    }
                    catch (IOException ex4) {
                        ex4.printStackTrace();
                    }
                    continue;
                }
                finally {
                    try {
                        if (writer != null) {
                            ((BufferedWriter)writer).close();
                        }
                        if (writer2 != null) {
                            ((OutputStreamWriter)writer2).close();
                        }
                    }
                    catch (IOException ex4) {
                        ex4.printStackTrace();
                    }
                }
                try {
                    if (writer != null) {
                        ((BufferedWriter)writer).close();
                    }
                    if (writer2 != null) {
                        ((OutputStreamWriter)writer2).close();
                    }
                }
                catch (IOException ex4) {
                    ex4.printStackTrace();
                }
                try {
                    if (writer != null) {
                        ((BufferedWriter)writer).close();
                    }
                    if (writer2 != null) {
                        ((OutputStreamWriter)writer2).close();
                    }
                }
                catch (IOException ex5) {
                    ex5.printStackTrace();
                }
            }
            else {
                System.out.println("No Records required to Insert ==================== ");
            }
        }
    }
    
    public int vrfyInsrt(final String s) {
        int n = 0;
        for (int i = 1; i < SQLVars.getFileData().size(); ++i) {
            if (SQLVars.getFileData().get(i).toString().split("\\|")[SQLVars.getColPos().get("SQL_CONTENT")].equalsIgnoreCase(s)) {
                ++n;
                i = SQLVars.getFileData().size() + 1;
            }
        }
        return n;
    }
    
    public void getSql(final String s) {
        final File file = new File(s);
        final ConcurrentHashMap<Integer, String> sqls = new ConcurrentHashMap<Integer, String>();
        int n = 0;
        final StringBuilder sb = new StringBuilder();
        try {
            this.b = new BufferedReader(new FileReader(file));
            String line;
            while ((line = this.b.readLine()) != null) {
                if (!line.trim().isEmpty()) {
                    if (line.trim().substring(0, 2).equalsIgnoreCase("--")) {
                        sb.append("###");
                    }
                    else {
                        sb.append(line.trim());
                        sb.append(" ");
                    }
                }
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        String[] split;
        for (int length = (split = sb.toString().split("###")).length, i = 0; i < length; ++i) {
            final String s2 = split[i];
            if (!s2.trim().isEmpty()) {
                sqls.put(n, s2.trim());
                ++n;
            }
        }
        SQLVars.setSqls(sqls);
    }
    
    public void getData() {
        final ArrayList<String> fileData = new ArrayList<String>();
        try {
            this.b = new BufferedReader(new FileReader(SQLVars.getFile()));
            String line;
            while ((line = this.b.readLine()) != null) {
                fileData.add(line);
            }
            SQLVars.setFileData(fileData);
            this.getColPosition();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public void getColPosition() {
        final ConcurrentHashMap<String, Integer> colPos = new ConcurrentHashMap<String, Integer>();
        final String[] split = SQLVars.getFileData().get(0).toString().split("\\|");
        for (int i = 0; i < split.length; ++i) {
            colPos.put(split[i], i);
        }
        SQLVars.setColPos(colPos);
    }
    
    public void runSQL(final String s) {
        final ArrayList<String> list = new ArrayList<String>();
        this.getData();
        for (int i = 1; i < SQLVars.getFileData().size(); ++i) {
            final String[] split = SQLVars.getFileData().get(i).toString().split("\\|");
            if (split[SQLVars.getColPos().get(s.toUpperCase())].equalsIgnoreCase("RUN")) {
                this.mkTstSh(split[SQLVars.getColPos().get("SQL_CONTENT")]);
                this.runShell("RUN");
                if (s.equalsIgnoreCase("SIT")) {
                    list.add(String.valueOf(String.valueOf(split[SQLVars.getColPos().get("SQL_SCRIPT")])) + "|" + "COMPLETED" + "|" + split[SQLVars.getColPos().get("UAT")] + "|" + split[SQLVars.getColPos().get("PREPROD")] + "|" + split[SQLVars.getColPos().get("PROD")] + "|" + split[SQLVars.getColPos().get("SQL_CONTENT")]);
                }
                else if (s.equalsIgnoreCase("UAT")) {
                    list.add(String.valueOf(String.valueOf(split[SQLVars.getColPos().get("SQL_SCRIPT")])) + "|" + split[SQLVars.getColPos().get("SIT")] + "|" + "COMPLETED" + "|" + split[SQLVars.getColPos().get("PREPROD")] + "|" + split[SQLVars.getColPos().get("PROD")] + "|" + split[SQLVars.getColPos().get("SQL_CONTENT")]);
                }
                else if (s.equalsIgnoreCase("PREPROD")) {
                    list.add(String.valueOf(String.valueOf(split[SQLVars.getColPos().get("SQL_SCRIPT")])) + "|" + split[SQLVars.getColPos().get("SIT")] + "|" + split[SQLVars.getColPos().get("UAT")] + "|" + "COMPLETED" + "|" + split[SQLVars.getColPos().get("PROD")] + "|" + split[SQLVars.getColPos().get("SQL_CONTENT")]);
                }
                else {
                    list.add(String.valueOf(String.valueOf(split[SQLVars.getColPos().get("SQL_SCRIPT")])) + "|" + split[SQLVars.getColPos().get("SQL_CONTENT")] + "|" + split[SQLVars.getColPos().get("SIT")] + "|" + split[SQLVars.getColPos().get("UAT")] + "|" + split[SQLVars.getColPos().get("PREPROD")] + "|" + "COMPLETED");
                }
            }
            else {
                list.add(SQLVars.getFileData().get(i).toString());
            }
        }
        this.updtCFG(list);
    }
    
    public void updtCFG(final List<String> list) {
        BufferedWriter bufferedWriter = null;
        OutputStreamWriter outputStreamWriter = null;
        this.setS2("SQL_SCRIPT|SIT|UAT|PREPROD|PROD|SQL_CONTENT");
        try {
            outputStreamWriter = new FileWriter(SQLVars.getFile().getAbsoluteFile(), false);
            bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write("SQL_SCRIPT|SIT|UAT|PREPROD|PROD|SQL_CONTENT");
            bufferedWriter.newLine();
            final Iterator<String> iterator = list.iterator();
            while (iterator.hasNext()) {
                bufferedWriter.write(iterator.next());
                bufferedWriter.newLine();
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            }
            catch (IOException ex2) {
                ex2.printStackTrace();
            }
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            }
            catch (IOException ex3) {
                ex3.printStackTrace();
            }
            return;
        }
        finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
                if (outputStreamWriter != null) {
                    outputStreamWriter.close();
                }
            }
            catch (IOException ex4) {
                ex4.printStackTrace();
            }
        }
        try {
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            if (outputStreamWriter != null) {
                outputStreamWriter.close();
            }
        }
        catch (IOException ex4) {
            ex4.printStackTrace();
        }
        try {
            if (bufferedWriter != null) {
                bufferedWriter.close();
            }
            if (outputStreamWriter != null) {
                outputStreamWriter.close();
            }
        }
        catch (IOException ex5) {
            ex5.printStackTrace();
        }
    }
    
    public void runShell(final String s) {
        try {
            String s2;
            if (s.equalsIgnoreCase("RUN")) {
                s2 = new String("/local_home/srcwgspg/sql_run/tst.sh");
            }
            else {
                s2 = new String("/local_home/srcwgspg/sql_run/rmtst.sh");
            }
            final Process exec = Runtime.getRuntime().exec(s2);
            exec.waitFor();
            final StringBuffer sb = new StringBuffer();
            String line;
            while ((line = new BufferedReader(new InputStreamReader(exec.getInputStream())).readLine()) != null) {
                sb.append(String.valueOf(String.valueOf(line)) + "\n");
            }
            System.out.println("### " + (Object)sb);
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    public void mkTstSh(final String s) {
        final File file = new File("tst.sh");
        final ArrayList<String> list = new ArrayList<String>();
        list.add("#!/bin/bash");
        list.add("sqlplus /nolog <<EOF");
        list.add("CONNECT BIX/Anthem#1@va10dx05-scan1.wellpoint.com:1525/WGSPEGART");
        list.add(s);
        list.add("EXIT;");
        list.add("EOF");
        Writer writer = null;
        Writer writer2 = null;
        try {
            writer2 = new FileWriter(file.getAbsoluteFile(), false);
            writer = new BufferedWriter(writer2);
            final Iterator<String> iterator = list.iterator();
            while (iterator.hasNext()) {
                writer.write(iterator.next());
                ((BufferedWriter)writer).newLine();
            }
        }
        catch (IOException ex) {
            ex.printStackTrace();
            try {
                if (writer != null) {
                    ((BufferedWriter)writer).close();
                }
                if (writer2 != null) {
                    ((OutputStreamWriter)writer2).close();
                }
            }
            catch (IOException ex2) {
                ex2.printStackTrace();
            }
            try {
                if (writer != null) {
                    ((BufferedWriter)writer).close();
                }
                if (writer2 != null) {
                    ((OutputStreamWriter)writer2).close();
                }
            }
            catch (IOException ex3) {
                ex3.printStackTrace();
            }
            try {
                if (writer != null) {
                    ((BufferedWriter)writer).close();
                }
                if (writer2 != null) {
                    ((OutputStreamWriter)writer2).close();
                }
            }
            catch (IOException ex4) {
                ex4.printStackTrace();
            }
            return;
        }
        finally {
            try {
                if (writer != null) {
                    ((BufferedWriter)writer).close();
                }
                if (writer2 != null) {
                    ((OutputStreamWriter)writer2).close();
                }
            }
            catch (IOException ex4) {
                ex4.printStackTrace();
            }
        }
        try {
            if (writer != null) {
                ((BufferedWriter)writer).close();
            }
            if (writer2 != null) {
                ((OutputStreamWriter)writer2).close();
            }
        }
        catch (IOException ex4) {
            ex4.printStackTrace();
        }
        try {
            if (writer != null) {
                ((BufferedWriter)writer).close();
            }
            if (writer2 != null) {
                ((OutputStreamWriter)writer2).close();
            }
        }
        catch (IOException ex5) {
            ex5.printStackTrace();
        }
        try {
            Runtime.getRuntime().exec(new String("/local_home/srcwgspg/sql_run/permissions.sh")).waitFor();
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    public static String getSqlcompleted() {
        return "COMPLETED";
    }
    
    public String getS() {
        return this.s;
    }
    
    public void setS(final String s) {
        this.s = s;
    }
    
    public static String getSqlrun() {
        return "RUN";
    }
    
    public String getS2() {
        return this.s2;
    }
    
    public void setS2(final String s2) {
        this.s2 = s2;
    }
}