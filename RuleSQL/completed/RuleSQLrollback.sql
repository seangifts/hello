#Change from Transformers 8.30.17 schema name customer

UNDO INTO STATION VALUES (13, 'Phoenix', 'AZ', 33, 112); 
UNDO INTO STATION VALUES (44, 'Denver', 'CO', 40, 105); 
UNDO INTO STATION VALUES (66, 'Caribou', 'ME', 47, 68);

#Change from Orion 8.30.17 schema name address
UNDO CREATE TABLE STATION 
(ID INTEGER PRIMARY KEY, 
CITY CHAR(20), 
STATE CHAR(2), 
LAT_N REAL, 
LONG_W REAL);

